/**
 * Created by Laura on 08/12/2016.
 */
var index = require('./app/index');
var dataBase = require('./app/firebase.js');
var schedule = require('node-schedule');

var jobVenues,jobClubes,jobTorneos,job;


var APP = {
    scheduleJobVenues: function () {
        try {
            var rule4am = new schedule.RecurrenceRule();
            rule4am.hour=4;
            jobVenues  = schedule.scheduleJob(rule4am, function(){

                console.log(new Date() + ' GetVenuesInfo!');
                index.getVenuesInfo();
            });

        } catch(ex) {
            console.log("cron pattern not valid");
        }

    },
    scheduleJobClubs: function () {
        try {
            var rule3am = new schedule.RecurrenceRule();
            rule3am.hour=3;
            jobClubes  = schedule.scheduleJob(rule3am, function(){
                console.log(new Date() + ' getClubsInfo!');
                index.getClubsInfo();
            });

        } catch(ex) {
            console.log("cron pattern not valid");
        }

    },
    scheduleJobMatches: function () {
        try {
            var rule = new schedule.RecurrenceRule();
            rule.minute=[0,30];
            jobTorneos  = schedule.scheduleJob(rule, function(){
                console.log(new Date() + ' scheduleJobMatches!');
                index.getAllInfo();
            });
        } catch(ex) {
            console.log("cron pattern not valid");
        }

    },
    scheduleJobMatchesBussyDays: function () {
        try {
            var rule = new schedule.RecurrenceRule();
            rule.dayOfWeek = [3,6,0];
            rule.minute=[15,45];
            jobTorneos  = schedule.scheduleJob(rule, function(){
                console.log(new Date() + ' JobMatchesBussyDays!');
                index.getAllInfo();
            });
        } catch(ex) {
            console.log("cron pattern not valid");
        }

    },
    init: function () {
        APP.scheduleJobClubs();
        APP.scheduleJobMatches();
        APP.scheduleJobVenues();
        APP.scheduleJobMatchesBussyDays();
    }
};

(function () {
    APP.init();
    //dataBase.clearFirebase();
    //dataBase.removeInFirebase('', 'matches');
    // index.saveTorneoData(2690);

    index.getAllInfo();
    index.getVenuesInfo();
    index.getClubsInfo();

})();


