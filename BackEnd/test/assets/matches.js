/**
 * Created by Laura on 21/01/2017.
 */



module.exports = {
    torneoStatus: _torneoStatus

};

var _torneoStatus = {
    notready: {
        torneo_info: {
            competitionID: '2679',
            competition: 'CAMPEONATO TERRITORIAL DE LIGA masculino ',
            category: 'PRIMERA INFANTIL MASCULINA',
            phase: 'FASE FINAL',
            group: 'Grupo: FINAL 1IM',
            type: 'LIGA 1 Vuelta',
            status: 'notready'
        },
        n_matches: 7,
        matches: {
            '20170505_2679_1GRUPOPRIMERO_3GRUPOPRIMERO': {
                torneo: '2679',
                jornada: '1',
                date: '05/05/2017 ',
                timestamp: 1493935200000,
                venue: '',
                result: '',
                descansa: false,
                teams: {},
                status: 'future'
            },
            '20170505_2679_2GRUPOPRIMERO_4GRUPOPRIMERO': {
                torneo: '2679',
                jornada: '1',
                date: '05/05/2017 ',
                timestamp: 1493935200000,
                venue: '',
                result: '',
                descansa: false,
                teams: {},
                status: 'future'
            },
            '20170506_2679_1GRUPOPRIMERO_4GRUPOPRIMERO': {
                torneo: '2679',
                jornada: '2',
                date: '06/05/2017 ',
                timestamp: 1494021600000,
                venue: '',
                result: '',
                descansa: false,
                teams: {},
                status: 'future'
            },
            '20170506_2679_2GRUPOPRIMERO_3GRUPOPRIMERO': {
                torneo: '2679',
                jornada: '2',
                date: '06/05/2017 ',
                timestamp: 1494021600000,
                venue: '',
                result: '',
                descansa: false,
                teams: {},
                status: 'future'
            },
            '20170507_2679_1GRUPOPRIMERO_2GRUPOPRIMERO': {
                torneo: '2679',
                jornada: '3',
                date: '07/05/2017 ',
                timestamp: 1494108000000,
                venue: '',
                result: '',
                descansa: false,
                teams: {},
                status: 'future'
            },
            '20170507_2679_3GRUPOPRIMERO_4GRUPOPRIMERO': {
                torneo: '2679',
                jornada: '3',
                date: '07/05/2017 ',
                timestamp: 1494108000000,
                venue: '',
                result: '',
                descansa: false,
                teams: {},
                status: 'future'
            }
        }
    },
    ended: {
        torneo_info: {
            competitionID: '2683',
            competition: 'CAMPEONATO TERRITORIAL DE LIGA masculino ',
            category: 'PRIMERA JUVENIL MASCULINA',
            phase: 'FASE PREVIA',
            group: 'Grupo: PREVIA 1JM',
            type: 'LIGA 1 Vuelta',
            status: 'ended'
        },
        n_matches: 7,
        matches: {
            '20160923_2683_155_559': {
                torneo: '2683',
                jornada: '1',
                date: '23/09/2016 18:30',
                timestamp: 1474648200000,
                venue: 'EL CERRO, P.M. (COSLADA)',
                result: '40-16',
                descansa: false,
                teams: {},
                status: 'played'
            },
            '20160923_2683_1784_552': {
                torneo: '2683',
                jornada: '1',
                date: '23/09/2016 18:30',
                timestamp: 1474648200000,
                venue: 'JOAQUIN BLUME, C.D. (T. ARDOZ)',
                result: '31-35',
                descansa: false,
                teams: {},
                status: 'played'
            },
            '20160924_2683_155_552': {
                torneo: '2683',
                jornada: '2',
                date: '24/09/2016 17:45',
                timestamp: 1474731900000,
                venue: 'EL CERRO, P.M. (COSLADA)',
                result: '32-24',
                descansa: false,
                teams: {},
                status: 'played'
            },
            '20160924_2683_559_1784': {
                torneo: '2683',
                jornada: '2',
                date: '24/09/2016 19:00',
                timestamp: 1474736400000,
                venue: 'VILLAVICIOSA DE ODON, P.M. (V. DE ODON)',
                result: '25-13',
                descansa: false,
                teams: {},
                status: 'played'
            },
            '20160925_2683_1784_155': {
                torneo: '2683',
                jornada: '3',
                date: '25/09/2016 18:30',
                timestamp: 1474821000000,
                venue: 'SONIA ABEJON, PABELLON (TORREJON DE ARDOZ)',
                result: '27-32',
                descansa: false,
                teams: {},
                status: 'played'
            },
            '20160925_2683_552_559': {
                torneo: '2683',
                jornada: '3',
                date: '25/09/2016 17:00',
                timestamp: 1474815600000,
                venue: 'LORENZO RICO, P.M. (COLMENAR VIEJO)',
                result: '33-30',
                descansa: false,
                teams: {},
                status: 'played'
            }
        },
    },
    ongoing: {
        torneo_info: {
            competitionID: '2805',
            competition: 'J.D.M. MADRID ',
            category: 'ALEVIN MIXTO',
            phase: '1&#xAA; FASE',
            group: 'Grupo: VILLAVERDE AX 713',
            type: 'Liga Flexible',
            status: 'ongoing'
        },
        n_matches: 13,
        matches: {
            '20161202_2805_1407_1111': {
                torneo: '2805',
                jornada: '1',
                date: '02/12/2016 18:30',
                timestamp: 1480699800000,
                venue: 'EL ESPINILLO, P.M.  (MADRID)',
                result: '20-21',
                descansa: false,
                teams: {},
                status: 'played'
            },
            '20161202_2805_1116_1106': {
                torneo: '2805',
                jornada: '1',
                date: '02/12/2016 17:45',
                timestamp: 1480697100000,
                venue: 'EL ESPINILLO, P.M.  (MADRID)',
                result: '9-6',
                descansa: false,
                teams: {},
                status: 'played'
            },
            '20161202_2805_1113_1854': {
                torneo: '2805',
                jornada: '1',
                date: '02/12/2016 17:00',
                timestamp: 1480694400000,
                venue: 'EL ESPINILLO, P.M.  (MADRID)',
                result: '8-10',
                descansa: false,
                teams: {},
                status: 'played'
            },
            '20161216_2805_1111_1854': {
                torneo: '2805',
                jornada: '2',
                date: '16/12/2016 17:00',
                timestamp: 1481904000000,
                venue: 'EL ESPINILLO, P.M.  (MADRID)',
                result: '',
                descansa: false,
                teams: {},
                status: 'waitingResult'
            },
            '20161216_2805_1106_1113': {
                torneo: '2805',
                jornada: '2',
                date: '16/12/2016 17:45',
                timestamp: 1481906700000,
                venue: 'EL ESPINILLO, P.M.  (MADRID)',
                result: '6-10 **',
                descansa: false,
                teams: {},
                status: 'played'
            },
            '20161216_2805_1407_1116': {
                torneo: '2805',
                jornada: '2',
                date: '16/12/2016 18:30',
                timestamp: 1481909400000,
                venue: 'EL ESPINILLO, P.M.  (MADRID)',
                result: '',
                descansa: false,
                teams: {},
                status: 'waitingResult'
            },
            '20170113_2805_1116_1111': {
                torneo: '2805',
                jornada: '3',
                date: '13/01/2017 17:00',
                timestamp: 1484323200000,
                venue: 'EL ESPINILLO, P.M.  (MADRID)',
                result: '6-23 **',
                descansa: false,
                teams: {},
                status: 'played'
            },
            '20170113_2805_1113_1407': {
                torneo: '2805',
                jornada: '3',
                date: '13/01/2017 17:45',
                timestamp: 1484325900000,
                venue: 'EL ESPINILLO, P.M.  (MADRID)',
                result: '11-13 **',
                descansa: false,
                teams: {},
                status: 'played'
            },
            '20170113_2805_1854_1106': {
                torneo: '2805',
                jornada: '3',
                date: '13/01/2017 18:30',
                timestamp: 1484328600000,
                venue: 'EL ESPINILLO, P.M.  (MADRID)',
                result: '5-10 **',
                descansa: false,
                teams: {},
                status: 'played'
            },
            '20170120_2805_1854_1407': {
                torneo: '2805',
                jornada: '4',
                date: '20/01/2017 17:00',
                timestamp: 1484928000000,
                venue: 'EL ESPINILLO, P.M.  (MADRID)',
                result: '',
                descansa: false,
                teams: {},
                status: 'waitingResult'
            },
            '20170120_2805_1116_1113': {
                torneo: '2805',
                jornada: '4',
                date: '20/01/2017 17:45',
                timestamp: 1484930700000,
                venue: 'EL ESPINILLO, P.M.  (MADRID)',
                result: '',
                descansa: false,
                teams: {},
                status: 'waitingResult'
            },
            '20170120_2805_1111_1106': {
                torneo: '2805',
                jornada: '4',
                date: '20/01/2017 18:30',
                timestamp: 1484933400000,
                venue: 'EL ESPINILLO, P.M.  (MADRID)',
                result: '',
                descansa: false,
                teams: {},
                status: 'waitingResult'
            }
        },
        teams: {
            '1106': 'DOS PARQUES',
            '1111': 'MONSERRAT',
            '1113': 'EL ESPINILLO',
            '1116': 'CIUDAD DE LOS ANGELES',
            '1407': 'CRISTOBAL COLON',
            '1854': 'JUAN SEBASTIAN EL CANO'
        },
        clasification: []
    },
    planned: {
        torneo_info: {
            competitionID: '2679',
            competition: 'CAMPEONATO TERRITORIAL DE LIGA masculino ',
            category: 'PRIMERA INFANTIL MASCULINA',
            phase: 'FASE FINAL',
            group: 'Grupo: FINAL 1IM',
            type: 'LIGA 1 Vuelta',
            status: 'planned'
        },
        n_matches: 7,
        matches: {
            '20170505_2679_1GRUPOPRIMERO_3GRUPOPRIMERO': {
                torneo: '2679',
                jornada: '1',
                date: '05/05/2017 ',
                timestamp: 1493935200000,
                venue: '',
                result: '',
                descansa: false,
                teams: {},
                status: 'future'
            },
            '20170505_2679_2GRUPOPRIMERO_4GRUPOPRIMERO': {
                torneo: '2679',
                jornada: '1',
                date: '05/05/2017 ',
                timestamp: 1493935200000,
                venue: '',
                result: '',
                descansa: false,
                teams: {},
                status: 'future'
            },
            '20170506_2679_1GRUPOPRIMERO_4GRUPOPRIMERO': {
                torneo: '2679',
                jornada: '2',
                date: '06/05/2017 ',
                timestamp: 1494021600000,
                venue: '',
                result: '',
                descansa: false,
                teams: {},
                status: 'future'
            },
            '20170506_2679_2GRUPOPRIMERO_3GRUPOPRIMERO': {
                torneo: '2679',
                jornada: '2',
                date: '06/05/2017 ',
                timestamp: 1494021600000,
                venue: '',
                result: '',
                descansa: false,
                teams: {},
                status: 'future'
            },
            '20170507_2679_1GRUPOPRIMERO_2GRUPOPRIMERO': {
                torneo: '2679',
                jornada: '3',
                date: '07/05/2017 ',
                timestamp: 1494108000000,
                venue: '',
                result: '',
                descansa: false,
                teams: {},
                status: 'future'
            },
            '20170507_2679_3GRUPOPRIMERO_4GRUPOPRIMERO': {
                torneo: '2679',
                jornada: '3',
                date: '07/05/2017 ',
                timestamp: 1494108000000,
                venue: '',
                result: '',
                descansa: false,
                teams: {},
                status: 'future'
            }
        }
    }

};
    
