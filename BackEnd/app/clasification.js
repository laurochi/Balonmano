/**
 * Created by Laura on 04/03/2017.
 */

var fs = require('fs');
var utils = require('./utils');
var q = require('q');
var dataBase = require('./firebase.js');


module.exports = {
    getClasification: getClasification

};
function getClasification(urlClasification, jsonTeams) {

    var json;
    var deferred = q.defer();
    var promises = [];
    utils.easyRequestPage(urlClasification)
        .then(function ($) {
            $('div.resultados table').eq(0).children('tr').each(function (i) {
                if (i != 0) {
                    json = {};
                    var team = $(this).children('.clasi_equipo').text().trim();

                    json = {
                        teamName: team,
                        //teamID: getTeamIDfromName(jsonTeams, team),
                        matchesPlayed: getClasificationNumberInColumn($(this), 0),
                        matchesWin: getClasificationNumberInColumn($(this), 1),
                        matchesTie: getClasificationNumberInColumn($(this), 2),
                        matchesLoss: getClasificationNumberInColumn($(this), 3),
                        goalsFavor: getClasificationNumberInColumn($(this), 4),
                        goalsAgainst: getClasificationNumberInColumn($(this), 5),
                        points: getClasificationNumberInColumn($(this), 6),
                        dif: getClasificationNumberInColumn($(this), 7)
                    };
                    promises.push(json);
                }
            });
            return q.all(promises);
        })
        .then(function (clasification) {
            deferred.resolve(clasification);
        });
return deferred.promise;
}


function getClasificationNumberInColumn(table, column) {
    var text = table.children('.clasi_columna').eq(column).text().trim();

    if (isNaN(Number(text))) {
        return 0;
    } else return Number(text);

}