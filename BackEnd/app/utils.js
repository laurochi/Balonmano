/**
 * Created by Laura on 18/12/2016.
 */
var request = require("request"), iconv = require('iconv-lite');
var cheerio = require('cheerio');
var q = require('q');
module.exports = {
    getIDRegex: getIDRegex,
    removePrefix: removePrefix,
    requestPage: requestPage,
    easyRequestPage: easyRequestPage,
    saveJSONFile: saveJSONFile,
    extractWithRegex: extractWithRegex,
    cleanJSON:cleanJSON
};


function getIDRegex(type, urlstring) {
    var regex = new RegExp(type + '=(.*)');
    var regexMatch = urlstring.match(regex);
    if (regexMatch) {
        return regexMatch[1];
    } else return ""
}

function removePrefix(prefix, urlstring) {
    var regex = new RegExp(prefix + '(.*)');
    var regexMatch = urlstring.match(regex);
    if (regexMatch) {
        return regexMatch[1];
    } else return ""
}

function extractWithRegex(regex, urlstring) {
    var regex = new RegExp(regex);
    var regexMatch = urlstring.match(regex);
    if (regexMatch) {
        return regexMatch[1];
    } else return ""
}


function requestPage(url, callback) {
    var requestOptions = {encoding: null, method: "GET", uri: url};

    request(requestOptions, function (error, response, body) {
            if (!error) {
                var utf8String = iconv.decode(new Buffer(body), "ISO-8859-1");
                var $ = cheerio.load(utf8String);
                var error = $('div img[src="http://www.federatio.com/500.png"]').length;
                if (error == 1) {
                    console.error('Federatio error:' + url);
                    var err = new Error('Sorry, page not found');
                    err.status = 404;
                    throw (err);
                    callback(err);
                } else {
                    callback(null, $);
                }

            } else {
                console.error('retry:' + url);
                //Retry
                request(requestOptions, function (error, response, body) {
                    if (!error) {
                        var utf8String = iconv.decode(new Buffer(body), "ISO-8859-1");
                        var $ = cheerio.load(utf8String);
                        var error = $('div img[src="http://www.federatio.com/500.png"]').length;
                        if (error == 1) {
                            console.error('Federatio error::' + url);
                            var err = new Error('Sorry, page not found');
                            err.status = 404;
                            callback(err);
                        } else {
                            callback(null, $);
                        }

                    } else {
                        console.error('Error in load:' + url);
                        var err = new Error('Sorry, page not found');
                        err.status = 404;
                        callback(err);
                    }
                });
            }
        }
    );
}

function easyRequestPage(url) {
    var requestOptions = {encoding: null, method: "GET", uri: url};
    var deferred = q.defer();
    request(requestOptions, function (error, response, body) {
        if (error) {
            console.error('utils, not found:' + url);
            deferred.reject();
        }

        var utf8String = iconv.decode(new Buffer(body), "ISO-8859-1");
        var $ = cheerio.load(utf8String);
        var errorcheck = $('div img[src="http://www.federatio.com/500.png"]').length;
        if (errorcheck === 1) {
            console.error('Federatio error:' + url);
            deferred.reject();
        } else {
           // console.log('call resolved:' + url);
            deferred.resolve($);
        }

    });
    return deferred.promise;
}


function saveJSONFile(name, json) {
    fs.writeFile('output/' + name + '.json', JSON.stringify(json, null, 4), function (err) {
     //   console.log('output/' + name + '.json File successfully written!');
    });
}

function cleanJSON(obj) {
    for (var propName in obj) {
        if (obj[propName] === null || obj[propName] === undefined || obj[propName] === "") {
            delete obj[propName];
        }
    }
    return obj;
}