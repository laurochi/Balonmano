/**
 * Created by Laura on 27/11/2016.
 */

// tools.js
// ========
var fs = require('fs');

module.exports = {
    initFirebaseApp: initFirebaseApp,
    setInFirebase: setInFirebase,
    updateInFirebase:updateInFirebase,
    removeInFirebase : removeInFirebase,
    getreferenceToDatabase:getreferenceToDatabase,
    clearFirebase:clearFirebase
};


var admin = require("firebase-admin");
var db;
var root='server/';

function initFirebaseApp () {
    var serviceAccount = require("./../firebase/Balonmano-69fb7ca1d94a.json");
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://balonmano-3181f.firebaseio.com"
    });
    return db = admin.database();
}
function getreferenceToDatabase(){
    return db;
}

function setInFirebase(json, ref, name) {
    var ref = db.ref(root+ref);
    var nameRef = ref.child(name);
    nameRef.set(json);
    
}

function updateInFirebase(json, ref, name) {
    var ref = db.ref(root+ref);
    var nameRef = ref.child(name);
    nameRef.update(json);
    
}

function removeInFirebase( ref, name) {
    var ref = db.ref(root+ref);
    var nameRef = ref.child(name);
    nameRef.remove();

}
function clearFirebase() {
    removeInFirebase('', 'clasifications');
    removeInFirebase('', 'clubs');
    removeInFirebase('', 'competitions');
    removeInFirebase('matches', 'byCLub');
    removeInFirebase('matches', 'byTOrneo');
    removeInFirebase('', 'teams');
    removeInFirebase('', 'torneo_info');
    removeInFirebase('', 'venues');
}


Date.prototype.addDays = function(days)
{
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
};

