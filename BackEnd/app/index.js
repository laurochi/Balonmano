/**
 * Created by Laura on 27/11/2016.
 */

var fs = require('fs');

var dataBase = require('./firebase.js');
var competition = require('./competition.js');
var clubs = require('./clubs.js');
var venues = require('./venues.js');

module.exports = {
    getAllInfo: getAllInfo,
    getClubsInfo: getClubsInfo,
    getVenuesInfo: getVenuesInfo,
    saveTorneoData: saveTorneoData
};

dataBase.initFirebaseApp();

var urlArbol = 'http://www.fmbalonmano.com/arbolN.asp';
var urlResults = 'http://www.fmbalonmano.com/resultados_competiciones.asp?torneo=';
var urlClasification = 'http://www.fmbalonmano.com/clasificaciones.asp?x_OPCION=MC&torneo=';
var urlClubs = 'http://www.fmbalonmano.com/clubes.asp';

var urlClubsDetail = 'http://www.fmbalonmano.com/clubes.asp?valor=';
var urlTeamsDetail = 'http://www.fmbalonmano.com/equipos.asp?valor=1&x_equipo_id=';
var urlVenues = 'http://fmbalonmano.com/terrenos.asp';
var urlVenuesDetail = 'http://fmbalonmano.com/mapa.asp?Campo=';

function saveTorneoData(torneo) {
    var urlResultsTorneo = urlResults + torneo;
    var urlClasificationTorneo = urlClasification + torneo;

    competition.getTorneoInfo(urlResultsTorneo, urlClasificationTorneo, torneo)
        .then(function ($) {
            //  console.log('torneo done!')
        })
        .catch(function () {
            console.error('ACTION_TO_SERVER_ERROR');
        });
}


function getAllInfo() {

    competition.getCompetitionsJSON(urlArbol, function (response) {
        if (response) {
            dataBase.setInFirebase(response, '', 'competitions');
        }
        for (var i = 0; i < response.torneosList.length; i++) {
            var torneo = response.torneosList[i];

            saveTorneoData(torneo);
        }
        saveTorneoData(2798);
    });

}

function getClubsInfo() {

    clubs.getAllCLubs(urlClubs, function (allClubs) {
        var clubsList = {};
        allClubs.forEach(function (club) {
            clubsList[club.url] = club.name;
        });

        allClubs.forEach(function (club) {
            clubs.getClubDetailedInfo(urlClubsDetail + club.url,club.url, function (clubInfo) {

                if (clubInfo.hasOwnProperty('teams')) {
                    if (Object.keys(clubInfo.teams).length !== 0) {

                        dataBase.setInFirebase(clubInfo, '', 'clubs/clubsInfo/' + club.url);

                        for (var keyTeam in clubInfo.teams) {
                            clubs.getTeamDetailedInfo(urlTeamsDetail + keyTeam, function (teamInfo) {
                                teamInfo.clubID = club.url;
                                teamInfo.clubName = club.name;
                                teamInfo.name = clubInfo.teams[keyTeam].name;
                                teamInfo.extName = clubInfo.teams[keyTeam].extName;
                                var itemTeamsList = {
                                    name: clubInfo.teams[keyTeam].name,
                                    extName: clubInfo.teams[keyTeam].extName,
                                    clubID: club.url,
                                    clubName: clubInfo.name,
                                    category: teamInfo.category
                                };
                                if (itemTeamsList) {
                                    dataBase.setInFirebase(itemTeamsList, '', 'teams/teamsList/' + keyTeam);
                                }
                                if (teamInfo) {
                                    dataBase.setInFirebase(teamInfo, '', 'teams/teamsInfo/' + keyTeam);
                                }
                            });
                        }
                    }
                }
            });
        });

    });
}


function getVenuesInfo() {

    venues.getAllVenues(urlVenues);

}





