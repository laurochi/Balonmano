/**
 * Created by Laura on 04/03/2017.
 */
var fs = require('fs');
var utils = require('./utils');
var q = require('q');
var dataBase = require('./firebase.js');
var clasification = require('./clasification.js');

module.exports = {
    getTorneoInfo: getTorneoInfo,
    getCompetitionsJSON: getCompetitionsJSON
};

function getTorneoInfo(urlTorneo, urlClasification, torneo) {

    var deferred = q.defer();
    clasification.getClasification(urlClasification).then(function (json) {
        for (key in json) {
            dataBase.updateInFirebase(json[key], 'clasifications/byTorneo/' + torneo, '' + key);
        }
    }).then(function () {
        utils.easyRequestPage(urlTorneo)
            .then(function ($) {
                extractCompetitionInfo($, torneo).then(function (torneo_info) {

                    dataBase.updateInFirebase(torneo_info, 'torneo_info/', torneo);
                    extractAllMatchesInfo($, torneo).then(function (matches) {

                        saveJornadasInfo(matches, torneo);


                        var jornadas = {};
                        for (var key in matches) {
                            var match = matches[key];

                            match.torneo_info = torneo_info;
                            dataBase.updateInFirebase(match, 'matches/byTorneo/' + torneo, '' + key);
                            if (match.teams.hasOwnProperty('local')) {
                                match.team = match.teams.local.teamID;
                                match.teamName = match.teams.local.name;
                                dataBase.updateInFirebase(match, 'matches/byClub/' + match.teams.local.clubID, '' + key);
                            }
                            if (match.teams.hasOwnProperty('visitor')) {
                                match.team = match.teams.visitor.teamID;
                                match.teamName = match.teams.visitor.name;
                                dataBase.updateInFirebase(match, 'matches/byClub/' + match.teams.visitor.clubID, '' + key);
                            }

                        }
                    });
                });
            });
    }).catch(function () {
        console.error('error');
    });


    return deferred.promise;
}

function extractCompetitionInfo($, competitionID) {
    var category, phase, group, type;
    var deferred = q.defer();
    var competition = $('div.competiciones h2').children('strong').text();
    var info = $('div.competiciones h2').html();
    if (info) {
        category = info.split("<br>")[1].trim();
        phase = info.split("<br>")[2].trim();
        group = info.split("<br>")[3].trim();
        type = info.split("<br>")[4].trim();
    } else {
    }
    deferred.resolve({
        competitionID: competitionID,
        competition: competition,
        category: category,
        phase: phase,
        group: group,
        type: type
    });
    return deferred.promise;
}


//* Helpers extract matches INfo*//
//gets all the info out of the venue page
function processNewVenuesPage($) {
    var deferred = q.defer();
    extractInfoVenues($)
        .then(function (venues) {
            var promises = [];

            for (var key in venues) {
                promises.push(
                    addVenueAddress(venues, key)
                );
            }
            return q.all(promises);
        })
        .then(function (venues) {
            deferred.resolve(venues[0]);
        });
    return deferred.promise;
}

function extractAllMatchesInfo($, torneo) {
    var deferred = q.defer();
    var matchesList = {};
    var promises = [];
    $('div.competiciones table').eq(0).children('tr').each(function (i) {
        if (i !== 0) {
            promises.push(
                extractMatchInfo($(this), torneo).then(function (match) {
                        var torneoKey = getGameID(match);
                        matchesList[torneoKey] = match;
                    }
                )
            );
        }
    });
    q.all(promises).then(function () {
        deferred.resolve(matchesList);
    });
    return deferred.promise;
}

function extractMatchInfo(page, torneo) {
    var deferred = q.defer();
    var match = {};
    var localURL, visitorURL;
    var jornada = page.children('td').eq(0).text().trim();
    var fecha = page.children('td').eq(3).text().trim();
    //TODO remove ñapa

    if (torneo === 4069 && jornada=='10' && fecha == '14/01/2017') {
        fecha = '14/01/2018';
    }


    localURL = page.children('td').eq(1).children().children().prop('href');
    visitorURL = page.children('td').eq(2).children().children().prop('href');
    var local = page.children('td').eq(1).children().children().text().trim();
    var visitor = page.children('td').eq(2).children().children().text().trim();
    if (local === '') {
        local = page.children('td').eq(1).children().text().trim();
    }
    if (visitor === '') {
        visitor = page.children('td').eq(2).children().text().trim();
    }

    var hora = page.children('td').eq(4).text().trim();
    var venue = page.children('td').eq(5).children('a').text().trim();
    var result = page.children('td').eq(6).text().trim();
    getTeams(local, localURL, visitor, visitorURL)
        .then(function (teams) {
            match = {
                torneo: torneo,
                jornada: jornada,
                date: fecha,
                hour: hora,
                venue: venue,
                result: result
            };
            match.teams = teams;
            if (result !== '') {
                var parts = result.split('-');
                match.teams.local.result = parts[0];
                match.teams.visitor.result = parts[1];
            }

            match.descansa = teams.isDescansa;

            if (match.descansa) {
                match.timestamp = parseDate(fecha);

            } else {
                match.date = (fecha + " " + match.hour).trim();
                match.timestamp = parseDate(fecha, match.hour);
            }
            var matchdate = new Date(match.timestamp);
            match.weekNumber = weekOfYear(matchdate);
            match.status = getMatchStatus(match);
            match = utils.cleanJSON(match);
            deferred.resolve(match);
        });
    return deferred.promise;
}

function extractTeamInfo(teamURL, local) {
    var json = {};
    var deferred = q.defer();
    if (teamURL) {
        json = {
            teamID: utils.getIDRegex('equipo_id', teamURL),
            clubID: utils.extractWithRegex('valor=(.*)&', teamURL),
            name: local
        };
        json = utils.cleanJSON(json);
        deferred.resolve(json);
    } else {

        json = {
            teamID: local.replace(/[^a-zA-z0-9]/g, ""),
            name: local.replace(/[^a-zA-z0-9]/g, ""),
            clubID: null
        };
        json = utils.cleanJSON(json);
        deferred.resolve(json);
    }

    return deferred.promise;
}

function getGameID(match) {
    var jornada=parseInt(match.jornada);
    if (jornada < 10) {
        jornada='0' + jornada;
    }
    var GameID = match.torneo + '_' + jornada + '_' + match.teams.local.teamID;

    if (match.teams.hasOwnProperty('visitor')) {
        return (GameID + '_' + match.teams.visitor.teamID);
    } else {
        return (GameID + '_000');
    }
}

function getMatchStatus(match) {

    if (match.descansa) {
        return 'descansa';
    }
    var now = new Date().getTime();

    if (now > match.timestamp) {
        if (match.result !== '') {
            return 'played';
        }
        if (match.result === '') {
            return 'waitingResult';
        }

    } else {
        var time = new Date(match.timestamp);
        var hour = time.getHours();
        var minute = time.getMinutes();
        if (hour === 0 && minute === 0) {
            return 'future';
        } else {
            return 'excheduled';
        }
    }


}

// parse a date in dd/mm/yyyy and time hh:hh
function parseDate(date, hour) {
    var parts = date.split('/');
    var myDate;
    // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
    if (hour) {
        var partshour = hour.split(':');
        myDate = new Date(parts[2], parts[1] - 1, parts[0], partshour[0], partshour[1]);
    } else {
        myDate = new Date(parts[2], parts[1] - 1, parts[0], 23,59);
    }

    return myDate.getTime(); // Note: months are 0-based
}

function getTeams(local, localURL, visitor, visitorURL) {
    var deferred = q.defer();
    var teams = {local: {}, visitor: {}, isDescansa: false};
    if (local === 'Descansa' && visitor === 'Descansa') {
        deferred.reject();
    }
    if (local === 'Descansa') {
        extractTeamInfo(visitorURL, visitor).then(function (team) {
            teams = {local: team, isDescansa: true};
            deferred.resolve(teams);
        });
    }
    if (visitor === 'Descansa') {
        extractTeamInfo(localURL, local).then(function (team) {
            teams = {local: team, isDescansa: true};
            deferred.resolve(teams);
        });
    }
    if (local !== 'Descansa' && visitor !== 'Descansa') {
        extractTeamInfo(localURL, local).then(function (localTeam) {
            teams.local = localTeam;
        }).then(function () {
            extractTeamInfo(visitorURL, visitor).then(function (visitorTeam) {
                teams.visitor = visitorTeam;
                deferred.resolve(teams);
            });
        });
    }
    return deferred.promise;
}


function getJSONCompetitions($) {
    var category, fase, group;
    var competitionsJSON = {competitions_tree: [], torneosList: {}};
    var groupIDS = [];

    $('div[class="toggle_container competicion"]').each(function (i, element) {

        var competition = $(this).prev().text().trim();
        var competitionShort = getCompetitionsShortName(competition);
        var categories1 = [];
        $(this).children('h3').each(function (i, element) {
            category = $(this).text().trim();
            var fases1 = [];
            $(this).nextUntil('h3', 'h6').each(function (i, element) {
                fase = $(this).first('strong').text().trim();
                var groups1 = [];
                $(this).nextUntil('h6', 'p').each(function (i, element) {
                    group = $(this).first('a').text().trim();
                    var torneoURL = $(this).children('a').eq(0).prop('href');
                    var grupoID;
                    if (torneoURL) {
                        grupoID = utils.getIDRegex('torneo', torneoURL);
                    } else {
                        grupoID = null;
                    }
                    groups1.push({
                        group: group,
                        torneoID: grupoID
                    });
                    groupIDS.push(grupoID);
                });

                fases1.push({
                    name: fase,
                    groups: groups1
                });

            });

            categories1.push({
                name: category,
                fases: fases1
            });
        });
        competitionsJSON.competitions_tree.push({
            name: competition,
            nameShort: competitionShort,
            categories: categories1
        });

        competitionsJSON.torneosList = (groupIDS);
    });
    return competitionsJSON;
}

function getCompetitionsJSON(url, callback) {
    utils.requestPage(url, function (error, $) {
        if (!error) {
            var json = getJSONCompetitions($);
            json = utils.cleanJSON(json);
            callback(json);
        }
    });
}

function getCompetitionsShortName(name) {
    name = name.replace("Femenino", "Fem.");
    name = name.replace("Masculino", "Masc.");
    name = name.replace("Campeonato", "C.");
    name = name.replace("De", "");
    name = name.replace("  ", " ");
    return name;
}

Date.prototype.getWeek = function () {
    var onejan = new Date(this.getFullYear(), 0, 1);
    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
};

function getDateOfISOWeek(w, y) {
    var simple = new Date(y, 0, 1 + (w - 1) * 7);
    var dow = simple.getDay();
    var ISOweekStart = simple;
    if (dow <= 4) {
        ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    } else {
        ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
    }
    return ISOweekStart;
}

function saveJornadasInfo(matches, torneo) {
    var jornadas = {};
    var date;
    var promises = [];
    for (var key in matches) {

        var index = matches[key].jornada;
        if (!jornadas.hasOwnProperty(index)) {
            var jornada = {};
            jornada.index = index;
            date = new Date(matches[key].timestamp);
            jornada.date = date;
            jornada.weekNumber = weekOfYear(date);
            jornada.timestamp = matches[key].timestamp;
            dataBase.updateInFirebase(jornada, 'torneo_info/' + torneo + '/jornadas', index);
        }
    }

    return;
}

function weekOfYear (date){
    var d = new Date(+date);
    d.setHours(0,0,0);
    d.setDate(d.getDate()+4-(d.getDay()||7));
    return Math.ceil((((d-new Date(d.getFullYear(),0,1))/8.64e7)+1)/7);
}


