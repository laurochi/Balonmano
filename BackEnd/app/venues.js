/**
 * Created by Laura on 08/12/2016.
 */

var fs = require('fs');
var utils = require('./utils');
var q = require('q');
var dataBase = require('./firebase.js');

module.exports = {
    getAllVenues: getAllVenues

};

function getAllVenues(urlVenues) {
    var deferred = q.defer();
    var pageVenue = urlVenues;
    dataBase.removeInFirebase('', 'venues');
    (function loop() {
            if (pageVenue !== 0) {

                utils.easyRequestPage(pageVenue)
                    .then(function ($) {
                            processNewVenuesPage($).then(function (json) {
                                dataBase.updateInFirebase(json, 'venues', 'venues_info');
                            });
                            getNextVenuePage($).then(function (newL) {
                                if (newL != 0) {
                                    pageVenue = urlVenues + newL;
                                    loop();
                                }
                            });
                        }
                    );
            }
        }()
    );

    return deferred.promise;
}

//Venues are paginated, this method gets the next page
function getNextVenuePage($) {
    var deferred = q.defer();
    $('form[name="form1"][action="terrenos.asp"]').siblings('strong').last().each(function (i) {

        if ($(this).text().indexOf('Next') > -1) {
            var link = utils.extractWithRegex('terrenos.asp(.*)', $(this).children('a').prop('href'));
            deferred.resolve(link);
        } else {
            deferred.resolve(0);
        }

    });
    return deferred.promise;
}

//gets all the info out of the venue page
function processNewVenuesPage($) {
    var deferred = q.defer();
    extractInfoVenues($)
        .then(function (venues) {
            var promises = [];

            for (var key in venues) {
                promises.push(
                    addVenueAddress(venues, key)
                );
            }
            return q.all(promises);
        })
        .then(function (venues) {
            deferred.resolve(venues[0]);
        });
    return deferred.promise;
}

//extracts the venue info from the html
function extractInfoVenues($) {
    var json = {name: '', town: '', venueID: '', map: ''};
    var allVenues = {};
    var deferred = q.defer();
    $('form[name="form1"][action="terrenos.asp"]').siblings('table').children('tr').each(function (i) {

        if (i > 0) {
            var venueID = utils.extractWithRegex('Campo=(.*)\'', $(this).children('td').eq(2).children('a').prop('href'));
            json = {
                name: $(this).children('td').eq(0).text().trim(),
                town: $(this).children('td').eq(1).text().trim(),
                map: $(this).children('td').eq(3).children('a').prop('href')
            };
            json=utils.cleanJSON(json);
            allVenues[venueID] = json;
        }
    });
    deferred.resolve(allVenues);
    return deferred.promise;

}
//makes the a new call to retrieve the venue address
function addVenueAddress(venues, id) {

    return utils.easyRequestPage('http://fmbalonmano.com/mapa.asp?Campo=' + id)
        .then(function ($) {
            venues[id].address = $('table').children('tr').eq(1).text().trim() + " - " + $('table').children('tr').eq(2).text().trim();
            return venues;
        });

}


