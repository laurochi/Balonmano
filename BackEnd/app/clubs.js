/**
 * Created by Laura on 08/12/2016.
 */

var fs = require('fs');
var utils = require('./utils');

module.exports = {
    getAllCLubs: getAllCLubs,
    getClubDetailedInfo: getClubDetailedInfo,
    getTeamDetailedInfo: getTeamDetailedInfo
};

function getAllCLubs(urlClubs, callback) {
    var allClubs = [];
    var itemsProcessed = 0;
    utils.requestPage(urlClubs, function (error, $) {
        if (!error) {
            processNewClubsPage(urlClubs, function (result) {
                allClubs = result;
                var lenght = $('table[class="tablagris"]').parent().next('div').children('strong').children('a').length;
                $('table[class="tablagris"]').parent().next('div').children('strong').children('a').each(function (i, element) {
                    var clubPageURL = $(this).prop('href');

                    if (clubPageURL && !$(this).text().match('Sig')) {
                        processNewClubsPage(urlClubs + clubPageURL, function (result2) {
                            allClubs.push.apply(allClubs, result2);
                            itemsProcessed++;
                            if (itemsProcessed === lenght) {
                                callback(allClubs);
                            }
                        });
                    } else {
                        itemsProcessed++;
                        if (itemsProcessed === lenght) {
                            callback(allClubs);
                        }
                    }
                });
            });

        }
    });
}

function processNewClubsPage(clubPage, callback) {
    utils.requestPage(clubPage, function (error, $) {
        if (!error) {
            extractInfoClubs($, function (result) {
                callback(result);
            });
        }
    });
}


function extractInfoClubs($, callback) {
    var json = {url: '', name: ''};
    var arrayJSON = [];
    $('table[class="tablagris"] tr td div a').each(function (i, element) {
        json = {
            url: utils.getIDRegex('valor', $(this).prop('href')),
            name: $(this).text().trim()
        };
        json=utils.cleanJSON(json);
        arrayJSON.push(json);
    });

    callback(arrayJSON);
}

function getClubDetailedInfo(url,clubID, callback) {
    var json = {};
    utils.requestPage(url, function (error, $) {
        if (!error) {
            var tableRows = $('table[class="tablacontenido"]');
            json.name = utils.removePrefix('CLUB: ', tableRows.siblings('h2').text().trim());
            json.nameShort=getClubShortName(json.name);
            json.representant = getValueInTablaContenido(tableRows, 0);
            json.address = getValueInTablaContenido(tableRows, 1);
            json.town = getValueInTablaContenido(tableRows, 2);
            json.email = getValueInTablaContenido(tableRows, 3);
            json.web = getValueInTablaContenido(tableRows, 4);
            json.clubID = clubID;
            json.teams = {};

            $('blockquote p').children('a').each(function (i, element) {
                var teamID = utils.getIDRegex('x_EQUIPO_ID', $(this).prop('href'));
                var text = $(this).text().trim();
                var team = {
                    name: utils.extractWithRegex('(?:-\\s)(.*)(?:\\s\\()', text),
                    extName: utils.extractWithRegex('(?:-\\s)(.*)', text)
                };
                team.category=getTeamCategory(team.extName);
                json.teams[teamID] = team;

            });
            json=utils.cleanJSON(json);
            callback(json);

        }
    });
}

function getValueInTablaContenido(tabla, row) {
    return tabla.children('tr').eq(row).children('td').eq(1).text().trim();
}

function getTeamDetailedInfo(url, callback) {
    var json = {};
    utils.requestPage(url, function (error, $) {
        if (!error) {
            var tableRows = $('table[class="tablacontenido"]');
            json.representant = getValueInTablaContenido(tableRows, 0);
            json.phone = getValueInTablaContenido(tableRows, 1);
            json.field = getValueInTablaContenido(tableRows, 2);
            json.fieldAddress = getValueInTablaContenido(tableRows, 3);
            json.category = getValueInTablaContenido(tableRows, 4);
            json.email = getValueInTablaContenido(tableRows, 5);
            json.equipacion1 = getValueInTablaContenido(tableRows, 6);
            json.equipacion2 = getValueInTablaContenido(tableRows, 7);
            json=utils.cleanJSON(json);
            callback(json);
         
        }
    });

}

function getTeamCategory(str){
    return str.substring(str.lastIndexOf("(")+1,str.lastIndexOf(")"));
}

function getClubShortName(name){
    name = name.replace("DEPORTIVA", "DEP.");
    name = name.replace("DEPORTIVO", "DEP.");
    name = name.replace("BALONMANO", "BM");
    name = name.replace("ELEMENTAL", "ELEM.");
    name = name.replace("FUNDACION", "FUND.");
    name = name.replace("  ", " ");
    return name;
}