import {Component, Input} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {TeamModel,ClubInfoModel} from '../../models/models';
/**
 * Generated class for the ClubInfoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    selector: 'club-info',
    templateUrl: 'club-info.html'
})
export class ClubInfoComponent {
    @Input() clubInfo: ClubInfoModel;
    text: string;

    constructor() {

    }

}
