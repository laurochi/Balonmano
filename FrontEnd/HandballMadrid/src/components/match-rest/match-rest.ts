import { Component,Input } from '@angular/core';
import {Observable} from 'rxjs/Observable';

/**
 * Generated class for the MatchRestComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'match-rest',
  templateUrl: 'match-rest.html'
})
export class MatchRestComponent {
    @Input() match: Observable<any[]>;
    @Input() showCategory: boolean = false;

  constructor() {

  }

}
