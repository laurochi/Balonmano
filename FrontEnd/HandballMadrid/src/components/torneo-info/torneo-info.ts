import { Component,Input } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable,FirebaseObjectObservable } from 'angularfire2/database';
/**
 * Generated class for the TorneoInfoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'torneo-info',
  templateUrl: 'torneo-info.html'
})
export class TorneoInfoComponent {
  @Input() torneoInfo : FirebaseObjectObservable<any>;

  constructor() {

  }

}
