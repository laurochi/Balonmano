import {Component, Input} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { ModalController } from 'ionic-angular';
import {ModalsPage} from '../../pages/modals/modals';
/**
 * Generated class for the MatchComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    selector: 'match',
    templateUrl: 'match.html'
})
export class MatchComponent {
    @Input() match: Observable<any[]>;
    @Input() showCategory: boolean = false;
    @Input() clubID: string = null;
    ModalsPage=ModalsPage;
    constructor(public modalCtrl : ModalController) {

    }
    public openModal(match){
        var modalPage = this.modalCtrl.create(this.ModalsPage,match, {showBackdrop: true, enableBackdropDismiss: true});
        modalPage.present();
    }
}
