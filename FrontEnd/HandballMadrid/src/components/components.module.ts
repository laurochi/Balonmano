import {NgModule} from '@angular/core';
import {MatchComponent} from './match/match';
import {MatchRestComponent} from './match-rest/match-rest';
import {IonicModule} from 'ionic-angular';
import { TorneoInfoComponent } from './torneo-info/torneo-info';
import { ClubInfoComponent } from './club-info/club-info';
import {PipesModule} from '../pipes/pipes.module';

@NgModule({
    declarations: [
        MatchComponent,
        MatchRestComponent,
    TorneoInfoComponent,
    ClubInfoComponent],
    imports: [IonicModule,PipesModule],
    exports: [
        MatchComponent,
        MatchRestComponent,
    TorneoInfoComponent,
    ClubInfoComponent],
    schemas: []
})
export class ComponentsModule {
}
