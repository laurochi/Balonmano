import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {AngularFireDatabase, FirebaseListObservable,FirebaseObjectObservable } from 'angularfire2/database';
import {ClubInfoModel} from "../../models/models";

/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FirebaseProvider {

    private root = 'server';

    constructor(private db: AngularFireDatabase) {
    }


    getCategories(competition): FirebaseListObservable<any> {
        return this.db.list('/' + this.root + '/competitions/competitions_tree/' + competition + '/categories');

    }

    getCompetitions(): FirebaseListObservable<any> {
        return this.db.list('/' + this.root + '/competitions/competitions_tree/');

    }

    getTorneoClasification(torneoID): FirebaseListObservable<any> {
        return this.db.list('/' + this.root + '/clasifications/byTorneo/' + torneoID);
    }

    getTorneoMatches(torneoID): FirebaseListObservable<any> {
        return this.db.list('/' + this.root + '/matches/byTorneo/' + torneoID);
    }

    getTorneoInfo(torneoID): FirebaseObjectObservable<any> {
        return this.db.object('/' + this.root + '/torneo_info/' + torneoID);
    }

    getTorneoJornadas(torneoID): FirebaseListObservable<any> {
        return this.db.list('/' + this.root + '/torneo_info/' + torneoID + '/jornadas/');
    }

//Clubs
    getClubMatches(clubID): FirebaseListObservable<any> {
        return this.db.list('/' + this.root + '/matches/byClub/' + clubID, {
            query: {
                orderByChild: 'timestamp'
            }
        });
    }

    getTeamMatches(clubID, teamID): FirebaseListObservable<any> {
        return this.db.list('/' + this.root + '/matches/byClub/' + '47', {
            query: {
                orderByChild: 'timestamp'
            }
        });
    }

    getClubs(): FirebaseListObservable<ClubInfoModel[]> {
        return <FirebaseListObservable<ClubInfoModel[]>>this.db.list('/' + this.root + '/clubs/clubsInfo', {
            query: {
                orderByChild: 'name'
            }
        });
    }

    getClubInfo(clubID): FirebaseObjectObservable<any> {
        return this.db.object('/' + this.root + '/clubs/clubsInfo/' + clubID);
    }



    getVenue(venueName): FirebaseListObservable<any> {
        return this.db.list('/' + this.root + '/venues/venues_info/', {
            query: {
                orderByChild: 'name',
                equalTo: venueName
            }
        });


    }
}