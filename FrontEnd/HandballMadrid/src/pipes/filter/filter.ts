import { Pipe, PipeTransform } from '@angular/core';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
/**
 * Generated class for the FilterPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  // transform(input:any, jornada:any):Observable<any[]> {
  //     if (input == null) //since the async is still working
  //         return null;
  //     if (typeof input[0] === "undefined") {
  //         return input;
  //     }
  //     if(!Array.isArray(input)) return input;
  //     jornada=1;
  //     var config='jornada'
  //     var propertyToCheck:string = !Array.isArray(config) ? config : config[0];
  //     return matches.map(_matches => _matches.filter(match => match.jornada = jornada));
  // }
  transform(items: any[], key:any,args: any): any {
      if (items == null) //since the async is still working
              return [];
          if (typeof items[0] === "undefined") {
              return items;
          }
      return items.filter(item => item[key]==args);
  }
}
