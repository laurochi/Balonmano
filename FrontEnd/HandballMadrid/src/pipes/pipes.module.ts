import { NgModule } from '@angular/core';
import { OrderBy } from './order-by/order-by';
import {CommonModule} from "@angular/common";
import { FilterPipe } from './filter/filter';
import { ShortCategoryPipe } from './short-category/short-category';
@NgModule({
	declarations: [OrderBy,
    FilterPipe,
    ShortCategoryPipe],
	imports: [CommonModule],
	exports: [OrderBy,
    FilterPipe,
    ShortCategoryPipe]
})
export class PipesModule {}
