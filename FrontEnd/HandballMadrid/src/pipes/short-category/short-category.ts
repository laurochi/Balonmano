import {Pipe, PipeTransform} from '@angular/core';

/**
 * Generated class for the ShortCategoryPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
    name: 'shortCategory',
})
export class ShortCategoryPipe implements PipeTransform {
    /**
     * Takes a value and makes it lowercase.
     */

    transform(value: string, ...args) {
        let result = value.toLowerCase();
        let tokens = result.split(" ");

        for (var i = 0; i < tokens.length; ++i) {
            let key=tokens[i];
            key = key.replace('(','');
            key = key.replace(')','');
            if(this.keys.hasOwnProperty(key)){
                result = result.replace(key, this.keys[key]);
            }

        }

        return result.toUpperCase();
    }

    keys = {
        'primera': '1º',
        'segunda': '2º',
        'tercera': '3º',
        'cuarta': '4º',
        'quinta': '5º',
        'sexta': '6º',
        'masculina': 'Mas',
        'femenina': 'Fem',
        'benjamin': 'Benj',
        'alevin': 'Alev',
        'infantil': 'Inf',
        'cadete': 'Cad',
        'juvenil': 'Juv',
        'territorial': 'Terri',
        'nacional': 'Nac',
        'veteranos': 'Vet',
    };
}
