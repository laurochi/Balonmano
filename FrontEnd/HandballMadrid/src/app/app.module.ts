import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {ComponentsModule} from '../components/components.module';
import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ClasificationPage} from '../pages/clasification/clasification';
import {TorneoPage} from '../pages/torneo/torneo';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {PipesModule} from '../pipes/pipes.module';
import {CategoriesPage} from '../pages/categories/categories';
import {ClubsPage} from "../pages/clubs/clubs";
import {FirebaseProvider} from '../providers/firebase/firebase.provider';
import {ClubDetailPage} from '../pages/club-detail/club-detail';
import {ClubMatchesPage} from '../pages/club-matches/club-matches';
import {CompetitionsPage} from '../pages/competitions/competitions';
import {ModalsPage} from '../pages/modals/modals';
import { LOCALE_ID } from '@angular/core';
@NgModule({
    declarations: [
        MyApp,
        HomePage,
        ClasificationPage,
        TorneoPage,
        CategoriesPage,
        ClubsPage,
        ClubDetailPage,
        ModalsPage,
        ClubMatchesPage, CompetitionsPage

    ],
    imports: [
        PipesModule,
        IonicModule.forRoot(MyApp),
        BrowserModule,
        HttpModule,
        AngularFireModule.initializeApp({
            apiKey: "AIzaSyAHRt1Qces2QbX_pSxBa7PodLo26E6RdLQ",
            authDomain: "balonmano-3181f.firebaseapp.com",
            databaseURL: "https://balonmano-3181f.firebaseio.com",
            projectId: "balonmano-3181f",
            storageBucket: "balonmano-3181f.appspot.com",
            messagingSenderId: "894709018565"
        }, 'balonmano'),
        AngularFireDatabaseModule,
        ComponentsModule

    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        ClasificationPage,
        TorneoPage,
        CategoriesPage,
        ClubsPage,
        ClubDetailPage,
        ModalsPage,
        ClubMatchesPage,
        CompetitionsPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        {provide: LOCALE_ID, useValue: "es-es" },
        FirebaseProvider
    ]
})

export class AppModule {

}
