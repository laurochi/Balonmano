import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {HomePage} from '../pages/home/home';
import {ClubsPage} from '../pages/clubs/clubs';
import {CompetitionsPage} from '../pages/competitions/competitions';
@Component({
    templateUrl: 'app.html',
    selector: 'app-root',
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;
    rootPage: any = HomePage;
    activePage :any;

    pages: Array<{ title: string, component: any }>;

    constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
        this.initializeApp();
        this.pages = [
            {title: 'Inicio', component: HomePage},
            {title: 'Competiciones', component: CompetitionsPage},
            {title: 'Clubs', component: ClubsPage}
        ];
        this.activePage= this.pages[0];
    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }
    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.activePage=page;
        this.nav.setRoot(page.component, page.title);
    }
    checkActive(page) {
       return page == this.activePage;
    }
}

