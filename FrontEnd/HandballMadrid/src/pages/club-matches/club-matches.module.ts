import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClubMatchesPage } from './club-matches';

@NgModule({
  declarations: [
    ClubMatchesPage,
  ],
  imports: [
    IonicPageModule.forChild(ClubMatchesPage),
  ],
})
export class ClubMatchesPageModule {}
