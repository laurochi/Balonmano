import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform, Slides} from 'ionic-angular';
import {FirebaseListObservable} from 'angularfire2/database';
import {TeamModel, ClubInfoModel} from '../../models/models';
import {FirebaseProvider} from '../../providers/firebase/firebase.provider';
import {ClubDetailPage} from '../club-detail/club-detail';
/**
 * Generated class for the ClubMatchesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-club-matches',
    templateUrl: 'club-matches.html',
})
export class ClubMatchesPage {
    matches: FirebaseListObservable<any>;
    public club: ClubInfoModel;
    @ViewChild('mySlider') slider: Slides;
    selectedSegment: string;
    slides: any;
    weeks:any[]=[];
    loaded: boolean = false;
    ClubDetailPage=ClubDetailPage;

    constructor(private firedbSvc: FirebaseProvider, public navParams: NavParams, private platform: Platform) {
        this.club = new ClubInfoModel(navParams.data);
        this.selectedSegment = 'second';
        const week= this.currentWeekNumber();
        this.weeks=[week-1,week,week+1];
        this.slides = [
            {
                id: "Última jornada",
            },
            {
                id: "Esta jornada",
            },
            {
                id: "Próxima jornada",
            }
        ];
    }

    doRefresh() {
        this.firedbSvc.getClubMatches(this.club.clubID).subscribe(res => {
            this.matches = res;
            this.loaded=true;
        });
    }

    ionViewDidLoad() {
        this.platform.ready()
            .then(() => {
                this.doRefresh();
            });
    }

    onSegmentChanged(segmentButton) {
        const selectedIndex = this.slides.findIndex((slide) => {
            return slide.id === segmentButton.value;
        });
        this.slider.slideTo(selectedIndex);
    }

    onSlideChanged(slider) {
        const currentSlide = this.slides[slider.getActiveIndex()];
        if (typeof currentSlide !== 'undefined') {
            this.selectedSegment = currentSlide.id;
        }
    }


     currentWeekNumber() {
        // Copy date so don't modify original
         const d: Date = new Date();
        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
        // Get first day of year
        let yearStart = +new Date(Date.UTC(d.getUTCFullYear(),0,1));
         const dnumber= +new Date(d);
        // Calculate full weeks to nearest Thursday
        let weekNo = Math.ceil(( ( (dnumber - yearStart) / 86400000) + 1)/7);
        // Return array of year and week number
        return weekNo;
    }
}
