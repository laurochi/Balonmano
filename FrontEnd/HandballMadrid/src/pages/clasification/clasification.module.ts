import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClasificationPage } from './clasification';

@NgModule({
  declarations: [
    ClasificationPage,
  ],
  imports: [
    IonicPageModule.forChild(ClasificationPage),
  ],
})
export class ClasificationPageModule {}
