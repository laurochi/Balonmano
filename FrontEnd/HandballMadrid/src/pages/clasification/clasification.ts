import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {FirebaseProvider} from '../../providers/firebase/firebase.provider';
import {TorneoInfoModel} from '../../models/models';
/**
 * Generated class for the ClasificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-clasification',
    templateUrl: 'clasification.html',
})
export class ClasificationPage {

    public clasification;
    torneoInfo : TorneoInfoModel;

    constructor(public navCtrl: NavController, private firedbSvc: FirebaseProvider, public navParams: NavParams, private platform: Platform) {
        this.torneoInfo =new TorneoInfoModel(navParams.data);
        console.log(this.torneoInfo.torneoID)
    }

    doRefresh(refresher) {

        this.firedbSvc.getTorneoClasification(this.torneoInfo.torneoID).subscribe(res=>{
                this.clasification=res;
                if(refresher != 0){
                    refresher.complete();
                }
            });

    }
    ionViewDidLoad() {
        this.platform.ready()
            .then(() => {
                this.doRefresh(0);
            });

    }

}
