import {Component} from '@angular/core';
import {NavController, Platform} from 'ionic-angular';
import {FirebaseProvider} from '../../providers/firebase/firebase.provider';
import 'rxjs/add/operator/map';
import {CategoriesPage} from '../categories/categories';

@Component({
    selector: 'page-competitions',
    templateUrl: 'competitions.html'
})
export class CompetitionsPage {

    public competitions;

    constructor(public navCtrl: NavController,
                private firedbSvc: FirebaseProvider,
                private platform: Platform) {
        this.platform.ready()
            .then(() => {
                this.competitions = this.firedbSvc.getCompetitions();
            });
    }

    public categoriesPage = CategoriesPage;

    ionViewDidLoad() {

    }
}
