import {Component} from '@angular/core';
import {NavController, Platform} from 'ionic-angular';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database' ;
import 'rxjs/add/operator/map';
import {ClubMatchesPage} from '../club-matches/club-matches';
import {TorneoPage} from '../torneo/torneo';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    favouriteClubs = [{
        "address" : "AVDA. ALFONSO XIII 127\r\n28016 MADRID\r\nMADRID",
        "clubID" : "47",
        "email" : "iboladoduran@gmail.com",
        "name" : "CLUB CORAZONISTAS",
        "nameShort" : "CLUB CORAZONISTAS",
        "representant" : "IGNACIO BOLADO DURAN",
        "teams" : {
            "188" : {
                "category" : "SEGUNDA NACIONAL MASCULINA",
                "extName" : "CORAZONISTAS (SEGUNDA NACIONAL MASCULINA)",
                "name" : "CORAZONISTAS"
            },
            "209" : {
                "category" : "PRIMERA INFANTIL MASCULINA",
                "extName" : "CORAZONISTAS (PRIMERA INFANTIL MASCULINA)",
                "name" : "CORAZONISTAS"
            },
            "276" : {
                "category" : "SEGUNDA CADETE MASCULINA",
                "extName" : "CORAZONISTAS \"A\" (SEGUNDA CADETE MASCULINA)",
                "name" : "CORAZONISTAS \"A\""
            },
            "335" : {
                "category" : "SEGUNDA INFANTIL",
                "extName" : "CORAZONISTAS (SEGUNDA INFANTIL)",
                "name" : "CORAZONISTAS"
            },
            "852" : {
                "category" : "SEGUNDA CADETE MASCULINA",
                "extName" : "CORAZONISTAS \"B\" (SEGUNDA CADETE MASCULINA)",
                "name" : "CORAZONISTAS \"B\""
            },
            "1033" : {
                "category" : "SEGUNDA TERRITORIAL MASCULINA",
                "extName" : "CORAZONISTAS (SEGUNDA TERRITORIAL MASCULINA)",
                "name" : "CORAZONISTAS"
            },
            "1184" : {
                "category" : "PRIMERA JUVENIL MASCULINA",
                "extName" : "CORAZONISTAS (PRIMERA JUVENIL MASCULINA)",
                "name" : "CORAZONISTAS"
            },
            "1364" : {
                "category" : "SEGUNDA CADETE FEMENINA",
                "extName" : "CORAZONISTAS (SEGUNDA CADETE FEMENINA)",
                "name" : "CORAZONISTAS"
            },
            "1508" : {
                "category" : "SEGUNDA JUVENIL MASCULINA",
                "extName" : "CORAZONISTAS (SEGUNDA JUVENIL MASCULINA)",
                "name" : "CORAZONISTAS"
            },
            "1513" : {
                "category" : "PRIMERA INFANTIL FEMENINA",
                "extName" : "CORAZONISTAS (PRIMERA INFANTIL FEMENINA)",
                "name" : "CORAZONISTAS"
            },
            "1539" : {
                "category" : "TERCERA TERRITORIAL MASCULINA",
                "extName" : "CORAZONISTAS (TERCERA TERRITORIAL MASCULINA)",
                "name" : "CORAZONISTAS"
            },
            "1651" : {
                "category" : "PRIMERA NACIONAL FEMENINA",
                "extName" : "CORAZONISTAS (PRIMERA NACIONAL FEMENINA)",
                "name" : "CORAZONISTAS"
            },
            "1804" : {
                "category" : "PRIMERA JUVENIL FEMENINA",
                "extName" : "CORAZONISTAS (PRIMERA JUVENIL FEMENINA)",
                "name" : "CORAZONISTAS"
            }
        },
        "town" : "MADRID",
        "web" : "www.bmcorasmad.es"
    }];
    favouriteTorneos = [
        {
            fase: "1ª Fase",
            group: "Grupo B",
            torneoID: "4069",
            competitionName: "C. Territorial Liga Fem.",
            categoryName: "Primera Nacional Femenina",
            "name": "Liga - 1NF - Grupo B",
        },
        {
            fase: "Fase Unica", group: "Unico",
            torneoID: "4014",
            competitionName: "C. Territorial Liga Masc.",
            categoryName: "Segunda Nacional Masculina",
            "name": "Liga - 2NM - Grupo Unico",
        }, {
            fase: "Fase Unica",
            group: "Grupo Unico",
            torneoID: "4088",
            competitionName: "C. Territorial Liga Masc.",
            categoryName: "Segunda Territorial Masculina",
            name: "Liga - 2TM - Grupo Unico"
        }
    ];
    public ClubMatchesPage = ClubMatchesPage;
    public TorneoPage = TorneoPage;

    constructor(public navCtrl: NavController,
                private angFire: AngularFireDatabase,
                private platform: Platform) {

    }


    ionViewDidLoad() {
        this.platform.ready()
            .then(() => {

            });
    }
}
