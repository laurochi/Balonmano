import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {FirebaseProvider} from '../../providers/firebase/firebase.provider';

import 'rxjs/add/operator/map';
import { TorneoPage } from '../torneo/torneo';
/**
 * Generated class for the CategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-categories',
    templateUrl: 'categories.html',
})
export class CategoriesPage {

    public categories;
    public competitionID;
    public competitionName;
    public torneoPage = TorneoPage;
    public catList;
    public loadedCat;
    public toggled: boolean = false;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private firedbSvc: FirebaseProvider,
                private platform: Platform) {
        this.competitionID = navParams.data.id;
        this.competitionName = navParams.data.name;
        this.toggled = false;
        console.log("Categories:"+this.competitionID+"/" +this.competitionName);
    }

    ionViewDidLoad() {
        this.platform.ready()
            .then(() => {
                this.catList = this.firedbSvc.getCategories(this.competitionID);
                this.categories = this.catList;
                this.loadedCat = this.catList;
        });
    }

    initializeItems(): void {
        this.categories = this.loadedCat;
    }

    searchThis(searchbar) {
        // Reset items back to all of the items
        this.initializeItems();
        // set q to the value of the searchbar
        let q = searchbar.srcElement.value;
        // if the value is an empty string don't filter the items
        if (!q) {
            return;
        }

        this.categories = this.categories.map(_cat => _cat.filter((v) => {
            if (v.name && q) {
                if (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
                    return true;
                }
                return false;
            }
        }));

    }
    public toggle(): void {
        this.toggled = this.toggled ? false : true;
    }
    public cancelSearch(): void {
        this.toggled = false;
    }
}