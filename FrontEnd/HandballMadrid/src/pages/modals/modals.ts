import {Component} from '@angular/core';
import {IonicPage, NavParams} from 'ionic-angular';
import {ViewController} from 'ionic-angular';
import {FirebaseProvider} from '../../providers/firebase/firebase.provider';

/**
 * Generated class for the ModalsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-modals',
    templateUrl: 'modals.html',
})
export class ModalsPage {
    match;
    venue;
    loaded: boolean = false;
    constructor(  private firedbSvc: FirebaseProvider, public navParams: NavParams, public viewCtrl: ViewController) {

        this.match = navParams.data;
        if (this.match.venue) {
            this.firedbSvc.getVenue(this.match.venue).subscribe(res => {
                this.venue = res[0];
                this.loaded=true;
            });
        }
    }


    public closeModal() {
        this.viewCtrl.dismiss();
    }
}
