import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TorneoPage } from './torneo';

@NgModule({
  declarations: [
    TorneoPage,
  ],
  imports: [
    IonicPageModule.forChild(TorneoPage),
  ],
})
export class TorneoPageModule {}
