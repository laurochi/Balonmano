import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform, Slides} from 'ionic-angular';
import 'rxjs/add/operator/map';
import {FirebaseProvider} from '../../providers/firebase/firebase.provider';
import {ClasificationPage} from '../clasification/clasification';
import {TorneoInfoModel} from '../../models/models';

/**
 * Generated class for the TorneoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-torneo',
    templateUrl: 'torneo.html',
})


export class TorneoPage {
    @ViewChild('mySlider') slider: Slides;

    numbers = [0,1,2];
    firstLoad = true;

    public torneo;
    clasificationTab: any = ClasificationPage;
    matches;
    torneoInfo: TorneoInfoModel;
    jornadasArray: any = [];
    currentJornada:number = 0;
    loaded: boolean = false;
    jornadasLoaded: boolean = false;
    constructor(public navCtrl: NavController, private firedbSvc: FirebaseProvider, public navParams: NavParams, private platform: Platform) {
        this.torneo = navParams.data.torneoID;
        console.log(this.torneo);
        this.torneoInfo = new TorneoInfoModel(navParams.data);
    }

    doRefresh() {
        this.firedbSvc.getTorneoJornadas(this.torneo).subscribe(snapshots=>{
            this.jornadasArray=snapshots;
            snapshots.forEach(snapshot => {
                if (this.isCurrentJornada(snapshot)){
                    this.currentJornada=+snapshot.index;
                    this.numbers = [this.currentJornada-1, this.currentJornada, this.currentJornada+1];
                    this.jornadasLoaded=true;
                }
                if (+snapshot.index==this.jornadasArray.length && !this.jornadasLoaded){
                    this.numbers = [this.jornadasArray.length, 1,2];
                    this.jornadasLoaded=true;
                }
            });
            this.firedbSvc.getTorneoMatches(this.torneo).subscribe(res=>{
                this.matches=res;
                this.loaded=true;
            });
        });
    }


    ionViewDidLoad() {
        this.platform.ready()
            .then(() => {
                this.doRefresh();
            });
    }



    isCurrentJornada(jornadaItem) {
        var dt = this.weekOfYear(new Date());
        return dt == jornadaItem.weekNumber;
    }


    loadPrev() {

        let newIndex = this.slider.getActiveIndex();
        newIndex++;
        if (this.numbers[0] === 1) {
            this.numbers.unshift(this.jornadasArray.length)
        } else {
            this.numbers.unshift(this.numbers[0] - 1);
        }
        this.numbers.pop();

        // Workaround to make it work: breaks the animation
        this.slider.slideTo(newIndex, 0, false);
    }

    loadNext() {
        if (this.firstLoad && this.numbers.length>1) {
            // Since the initial slide is 1, prevent the first
            // movement to modify the slides
            this.firstLoad = false;
            return;
        }
        let newIndex = this.slider.getActiveIndex();

        newIndex--;
        if (this.numbers[1] === this.jornadasArray.length-1) {
            this.numbers.push(1)
        } else {
            this.numbers.push(this.numbers[this.numbers.length - 1] + 1);
        }
        this.numbers.shift();

        this.slider.slideTo(newIndex, 0, false);
    }

    weekOfYear (date){
        let d = new Date(+date);
        d.setHours(0,0,0);
        d.setDate(d.getDate()+4-(d.getDay()||7));
        return Math.ceil((((+d- +new Date(d.getFullYear(),0,1))/8.64e7)+1)/7);
    }

    getJornadaDate(index){
        let jornada=this.jornadasArray[index];
        console.log(jornada);
        return this.getSundayFromWeek(jornada.weekNumber,jornada.timestamp);
    }

    getSundayFromWeek(week,timestamp){
        var year=new Date(timestamp).getFullYear();
        var sunday = new Date(year, 0, (1 + (week) * 7));
        while (sunday.getDay() !== 0) {
            sunday.setDate(sunday.getDate() - 1);
        }
        return this.getFormattedDate(sunday);
    }

    getFormattedDate(date){
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        return [day, month, year].join('/');
    }
}