import {Component} from '@angular/core';
import {IonicPage, NavParams, Platform} from 'ionic-angular';
import 'rxjs/add/operator/map';
import {FirebaseProvider} from '../../providers/firebase/firebase.provider';
import { FirebaseListObservable } from 'angularfire2/database';
import {TeamModel,ClubInfoModel} from '../../models/models';
import * as _ from 'lodash';
import {Observable} from "rxjs/Observable";
/**
 * Generated class for the ClubDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-club-detail',
  templateUrl: 'club-detail.html',
})


export class ClubDetailPage {
    _: any = _;

    public club: ClubInfoModel;
    clubs;
    matches: FirebaseListObservable<any> ;

    constructor( private firedbSvc: FirebaseProvider, public navParams: NavParams, private platform: Platform) {
        this.club = new ClubInfoModel(navParams.data);

        if (this.club.nameShort==null){
            this.clubs =this.firedbSvc.getClubInfo(this.club.clubID);
        }
    }
    ionViewDidLoad() {
        this.platform.ready()
            .then(() => {

            });
    }

}
