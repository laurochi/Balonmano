import {Component} from '@angular/core';
import {IonicPage, NavParams, Platform} from 'ionic-angular';
import {FirebaseProvider} from '../../providers/firebase/firebase.provider';
import {ClubMatchesPage} from '../club-matches/club-matches';
import {Observable} from "rxjs/Observable";
import {ClubInfoModel} from '../../models/models';
/**
 * Generated class for the ClubsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-clubs',
    templateUrl: 'clubs.html',
})

export class ClubsPage {

    public clubs : Observable<ClubInfoModel[]>;
    public clubsList: Observable<ClubInfoModel[]>;
    public loadedClubs : Observable<ClubInfoModel[]>;
    public ClubMatchesPage = ClubMatchesPage;

    searchQuery: '';

    constructor(
                private firedbSvc: FirebaseProvider,
                private platform: Platform,
                public navParams: NavParams) {

    }

    ionViewDidLoad() {
        this.platform.ready()
            .then(() => {
                this.clubsList = this.firedbSvc.getClubs();
                this.loadedClubs = this.clubsList;
                this.clubs = this.clubsList;
            });

    }

    initializeItems(): void {
        this.clubs = this.loadedClubs;
    }

    getItems(searchbar) {
        // Reset items back to all of the items
        this.initializeItems();
        // set q to the value of the searchbar
        let q = searchbar.srcElement.value;
        // if the value is an empty string don't filter the items
        if (!q) {
            return;
        }

        this.clubs = this.clubs.map(_clubs => _clubs.filter((v) => {
            if (v.name && q) {
                if (v.name.toLowerCase().indexOf(q.toLowerCase()) > -1) {
                    return true;
                }
                return false;
            }
        }));

    }

}
