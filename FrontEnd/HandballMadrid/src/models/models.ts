export class ClubModel {
    name: string;

    constructor(name, age) {
        this.name = name;
    }
}
export class TorneoInfoModel {
    fase: string;
    group: string;
    torneoID: string;
    competitionName:string;
    categoryName:string;
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export class TeamModel {
    category: string;
    extName: string;
    name: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
export class ClubInfoModel {
    clubID: string;
    nameShort: string;
    name: string;
    town : string;
    representant : string;
    email :string;
    address:string;
    teams:{};

    constructor(values: Object = {}) {
        Object.assign(this, values);

    }
}